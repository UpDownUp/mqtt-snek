/* global mqtt */ // for the editor to know what mqtt is
console.log("I LIVE!");
//console.log(mqtt);

////////////////////////////// RUN AT START

let turn = 0;
let score = 0;
let mqtt_broker = "wss://test.mosquitto.org:8081";
// I/O is from server's perspective
let mqtt_input_topic = "/espsnek/butts";
let mqtt_output_topic = "/espsnek/dots";

// modified in index.html, left alone otherwise
let client;
let grid;

const currentUrl = window.location.href;
if (currentUrl.includes("index.html") || currentUrl.endsWith("/")) {
  // Only run all this on the main page

  grid = document.getElementById("binary-grid");
  // Populate empty grid with blank dots
  for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
      const dot = document.createElement("div");
      dot.classList.add("dot");
      grid.appendChild(dot);
    }
  }

  connectToBroker();
}

let dark = false;
//toggle_dark(dark);

////////////////////////////// FUNCTIONS

// dark
function toggle_dark(target) {
  const root = document.documentElement;
  if (target == false) {
    dark = false;
    root.style.setProperty("--color-bg", "#F4E285");
    root.style.setProperty("--color-text", "black");
  } else {
    dark = true;
    root.style.setProperty("--color-bg", "#024F4F");
    root.style.setProperty("--color-text", "#FEFAE0");
  }
}

//--------- MQTT connect and handlers
function connectToBroker() {
  client = mqtt.connect(mqtt_broker);
  client.on("connect", onConnect);
  client.on("error", onError);
  client.on("message", onMessage);
  client.on("offline", onOffline);
}

function onConnect() {
  console.log("Connected to MQTT broker");
  const statusText = document.getElementById("statusText");
  statusText.textContent = "🟠 Connected to server, subscribing to display...";

  // Subscribe to gamestate topic
  client.subscribe(mqtt_output_topic, (err, granted) => {
    if (!err) {
      // client.publish(mqtt_output_topic, "Hello mqtt");
      console.log("Successfully subscribed to snek dot matrix topic!");
      statusText.textContent =
        "🟢 Subscribed! Use arrow keys or WASD to steer.";
    } else {
      console.log("Error subscribing:", err);
    }
  });
}

function onError(error) {
  console.error("Error:", error);
  const statusText = document.getElementById("statusText");
  statusText.textContent = "🔴 Disconnected, attempting to connect...";
}

function onOffline() {
  console.log("Offline.");
  const statusText = document.getElementById("statusText");
  statusText.textContent = "🔴 Disconnected, attempting to connect...";
}

function onMessage(topic, message) {
  // message is Buffer
  const buffer = message.toString();
  console.log(buffer);

  // filter out potential noise
  if (buffer.length >= 16) {
    // translate string back into byte array
    const bytes = [];
    for (let i = 0; i < 12; i++) {
      const byte = parseInt(buffer.substr(i * 2, 2), 16);
      bytes.push(byte);
    }

    // send to parse
    //console.log(bytes);
    parse_byte_array(bytes);
  } else {
    console.warn("Invalid message:", buffer);
  }
}

//--------- GAME STATE PARSING
function parse_byte_array(bytes) {
  // Byte 9, 10: Parse turn
  // LSB and MSB, respectively.
  if (!isNaN(bytes[9]) && !isNaN(bytes[10])) {
    let maybeturn = bytes[9] & (0xff + (bytes[10] << 8)) & 0xff;

    // Turn parsing happens first, so if
    // an outdated turn is received, it's ignored.
    if (maybeturn > turn || maybeturn < 5) {
      turn = maybeturn;
    } else {
      console.warn(
        "Discarding RX'd turn " +
          maybeturn +
          " (" +
          bytes[9] +
          " MSB, " +
          bytes[10] +
          " LSB), (latest " +
          turn +
          ") from buffer: ",
        bytes.toString()
      );
      return;
    }
  } else {
    // Only the game over screen may omit bytes[9,10].
    turn = 0;
  }

  // Byte 0 - 7: Parse map grid
  for (let row = 0; row < 8; row++) {
    // paint each row as per each byte
    for (let col = 0; col < 8; col++) {
      const index = row * 8 + col;
      const dotState = bytes[row] & (1 << col);
      const dot = grid.children[index];
      dot.style.backgroundColor = dotState ? "#F4A259" : "#5B8E7D";
      // snake color : "blank" color
    }
  }

  // Byte 8: Parse fruit x,y.
  // Server side sends fruit pos on a 1-8 range.
  if (!isNaN(bytes[8]) && bytes[8] != 0) {
    let byte = bytes[8];
    let xpos = ((byte >> 4) & 0x0f) - 1;
    let ypos = (byte & 0x0f) - 1;
    let index = ypos * 8 + xpos;
    //console.log(xpos + "  " + ypos);
    const dot = grid.children[index];
    dot.style.backgroundColor = "#BC4B51";
    // fruit color
  }

  // Byte 11: Score
  if (!isNaN(bytes[11]) && bytes[11] != 0) {
    score = bytes[11]; // this one is simple
  }
}

//--------- KEYBOARD CONTROLS
document.addEventListener("keydown", function (event) {
  if (client) {
    if (event.key === "W" || event.key === "w" || event.key === "ArrowUp") {
      event.preventDefault();
      client.publish(mqtt_input_topic, "U");
    } else if (
      event.key === "A" ||
      event.key === "a" ||
      event.key === "ArrowLeft"
    ) {
      event.preventDefault();
      client.publish(mqtt_input_topic, "L");
    } else if (
      event.key === "S" ||
      event.key === "s" ||
      event.key === "ArrowDown"
    ) {
      event.preventDefault();
      client.publish(mqtt_input_topic, "D");
    } else if (
      event.key === "D" ||
      event.key === "d" ||
      event.key === "ArrowRight"
    ) {
      event.preventDefault();
      client.publish(mqtt_input_topic, "R");
    }
  }

  if (event.key === "K" || event.key === "k") {
    event.preventDefault();
    toggle_dark(!dark);
  }
});
