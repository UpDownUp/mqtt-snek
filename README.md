# What is this?

This repository serves as a snapshot for the mqtt-snek client. The [Glitch editor](https://glitch.com/edit/#!/mqtt-snek) ought to have a more up-to-date version.

Hosted on [mqtt-snek.glitch.me](https://mqtt-snek.glitch.me/).
For the server code, see [esp-snake](https://gitlab.com/UpDownUp/esp-snake).
